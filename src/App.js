import React ,{Fragment} from 'react';
import Nav from './Components/Nav';
import './App.css';
import Body from './Components/Body';
import Footer from './Components/Footer';

function App() {
  return (
    <Fragment className="">
<Nav/>
<Body/>
<Footer/>
    </Fragment>
  );
}

export default App;
