import React from 'react';
import '../App.css';

function body() {
    return(
        <body className="Body">           
            <div className="Text">
                <h1 className="H1">The Memory of Christmas...</h1>
                <p>Christmas can't be bought from a store . . . That's why we're here.</p>
                <p>Vacation. Decorations. Lights. Mince Pies. Gifts.</p>
                <p>We provide much more for a special holiday.</p>
                <button>Find out more</button>
            </div>
        </body>
        
    )
}
export default body;