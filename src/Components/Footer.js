import React from "react";
import '../App.css';

function footer() {
    return(
        <div>
            <ul className="Footer">
                <li className="FooterItems TrustedBy">TRUSTED BY:</li>
                <li className="FooterItems"><img src={require('../../src/airbnb.png')} alt="Logo" width="100px" /></li>
                <li className="FooterItems"><img src={require('../../src/hotelsng.png')} alt="Logo" height="60px" /></li>
                <li className="FooterItems"><img src={require('../../src/Holiday_Inn.png')} alt="Logo" height="50px" /></li>
            </ul>
            
        </div>
        
    )
}
export default footer;