import React from "react";
import '../App.css';

function nav() {
    return(
        <nav className="Nav">
            <p className="Logo">LOGO</p>
            <ul className="Nav">
                <li className="Menu1">About</li>
                <li className="Menu2">Destinations</li>
                <li className="Menu3">Schedules</li>
                <li className="Menu4">Contact Us</li>
            </ul>
        </nav>
        
        
    );

}
export default nav;